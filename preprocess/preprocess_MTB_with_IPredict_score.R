# library(tidyr)
library(openxlsx)
# library(dplyr)
library(stringr)
library(plyr)

set.seed(123)

df_MTB <- read.xlsx("data/preprocessed_MTB_data.xlsx")
mutations <- df_MTB[,20:377]

# turn everything to numeric
df_MTB$Sex <- as.numeric(as.factor(df_MTB$Sex))-1 # male: 1, female: 0
df_MTB$Number.of.prior.lines.of.therapy <- as.numeric(as.factor(df_MTB$Number.of.prior.lines.of.therapy))-1 # <3: 0, >=3: 1
for (i in 8:11){
  df_MTB[,i] <- as.numeric(df_MTB[,i])
}
df_MTB$Diagnosis <- as.factor(df_MTB$Diagnosis)
diagnosis <- data.frame(model.matrix( ~ Diagnosis - 1, data=df_MTB))
df_MTB$Compliance.with.recommendation.of.MTB <- as.factor(df_MTB$Compliance.with.recommendation.of.MTB)
Compliance.with.recommendation.of.MTB <- data.frame(model.matrix( ~ Compliance.with.recommendation.of.MTB - 1, data=df_MTB))
# df_MTB$Age <- as.factor(findInterval(df_MTB$Age, c(0,25,50,75)))
# Age <- data.frame(model.matrix( ~ Age - 1, data=df_MTB))
df_MTB$Age <- df_MTB$Age/100
df_MTB$Matched.treatment[df_MTB$Matched.treatment != "none"] <- 1
df_MTB$Matched.treatment[df_MTB$Matched.treatment == "none"] <- 0
df_MTB$Matched.treatment <- as.numeric(df_MTB$Matched.treatment)
base_features <- df_MTB[,c(2,3,5:11,19)]
colnames(base_features) <- c("Age","Sex", "GI", "Number_of_lines", "Immnunotherapy", "MS1", "MS2", "MS3", "MS4", "Matched.treatment")
base_features <- cbind(base_features, diagnosis, Compliance.with.recommendation.of.MTB)

# preprocess drugs
drug_names <- c()
for (i in 1:nrow(df_MTB)){
  temp <- str_split(df_MTB$Treatment.regimen.after.MTB[i], ", ", simplify = TRUE)
  for (j in temp){
    drug_names <- c(drug_names, j)
  }
}
drug_names <- unique(drug_names)
drug_mab <- drug_names[str_detect(drug_names, "mab|trial|inhibitor|Inhibitor")]
drug_no_mab <- drug_names[! str_detect(drug_names, "mab|trial|inhibitor|Inhibitor")]
drug_names <- c(drug_no_mab, drug_mab)
drugs <- data.frame(matrix(0L, ncol = length(drug_names), nrow = nrow(df_MTB)))
colnames(drugs) <- drug_names
for (i in 1:nrow(df_MTB)){
  temp <- str_split(df_MTB$Treatment.regimen.after.MTB[i], ", ", simplify = TRUE)
  for (j in temp){
    drugs[i,j] <- 1
  }
}
colnames(drugs) <- make.names(drug_names, unique=TRUE)
colnames(mutations) <- str_replace(colnames(mutations), "-|/", "")

Study.ID <- df_MTB$Study.ID
PFS.OS <- df_MTB[,13:16]
df <- cbind(Study.ID, PFS.OS, base_features, drugs, mutations)

write.csv(df, "data/one_hot_MTB_data.csv", row.names = FALSE)

df2 <- read.xlsx("data/MTBpaperAggregatedCat2NonITMono1.xlsx", sheet="Cat123NonITMono", startRow=2)
df2 <- df2[-c(1,2),]
Study.ID.with.IPredict.score <- df2$Study.ID
df_with_Ipredict_score <- data.frame(matrix(0L, ncol = length(df), nrow = nrow(df2)))
colnames(df_with_Ipredict_score) <- colnames(df)
for (i in 1:length(Study.ID.with.IPredict.score)){
  df_with_Ipredict_score[i,] <- df[which(df$Study.ID==Study.ID.with.IPredict.score[i]),]
}
features <- df2[,17:23]
features[is.na(features)] <- 0
features[,2] <- -as.numeric(features[,2])
features$Number.of.total.targets.hit <- as.numeric(features$Number.of.total.targets.hit)/7
features$Synergy <- as.numeric(features$Synergy)/3
features$Modified.number.of.targets.hit <- features$Modified.number.of.targets.hit/7
features$Redundancy <- as.numeric(features$Redundancy)
features$`ER+,.AR+` <- as.numeric(features$`ER+,.AR+`)
df_with_Ipredict_score <- c(df_with_Ipredict_score, features)
write.csv(df_with_Ipredict_score, "data/one_hot_MTB_data_with_Ipredict_score.csv", row.names = FALSE)
