library(survival)
library(survminer)
library(Rtsne)
library(ggplot2)
library(cluster)
library(openxlsx)
library(tidyverse)

tsne_plot <- function(x, clutering){
  tsne_obj <- Rtsne(x, perplexity=20, check_duplicates = False)
  tsne_data <- tsne_obj$Y %>%
    data.frame() %>%
    setNames(c("X", "Y")) %>%
    mutate(cluster = factor(clutering))
  ggplot(aes(x = X, y = Y), data = tsne_data) + 
    geom_point(aes(color = cluster))
}


km.HR <- function(logrank, alpha=.95){
  HR <- (logrank[["obs"]][1]/logrank[["exp"]][1])/(logrank[["obs"]][2]/logrank[["exp"]][2])
  SE_lnHR <- sqrt(1/logrank[["exp"]][1] + 1/logrank[["exp"]][2])
  CI_high <- exp(log(HR)+qnorm(alpha)*SE_lnHR)
  CI_low <- exp(log(HR)-qnorm(alpha)*SE_lnHR)
  results <- list(HR=HR, CI_low=CI_low, CI_high=CI_high)
  return(results)
}

HR <- function(HR, SE_lnHR, alpha=.95){
  CI_high <- round(exp(log(HR)+qnorm(alpha)*SE_lnHR),2)
  CI_low <- round(exp(log(HR)-qnorm(alpha)*SE_lnHR),2)
  results <- list(HR=HR, CI_low=CI_low, CI_high=CI_high)
  print(CI_low)
  print(CI_high)
}


km.p <- function(logrank, df=1){
  p <- pchisq(logrank$chisq, df, lower.tail = FALSE)
  return(p)
}


# surplot <- function(km_results, data, logrank, alpha=.95){
#   HR <- km.HR(logrank, alpha)
#   p <- km.p(logrank)
#   ggsurv <- ggsurvplot(km_results, data = data, conf.int = TRUE)
#   ggsurv$plot <- ggsurv$plot +
#     ggplot2::annotate(
#       "text",
#       x = Inf, y = Inf,
#       vjust = 1, hjust = 1,
#       label = paste('HR = ', round(HR$HR,2), '\n', round(100*alpha), '%CI = (', round(HR$CI_low,2), ',', round(HR$CI_high,2), ')\n p = ', round(p,4), sep = ""),
#       size = 5
#     )
#   ggsurv
# }


surplot <- function(func, data, conf.type = "log-log", alpha=.95, conf.int = TRUE){
  km <- survfit(formula = func, data = data, conf.type = conf.type, conf.int = alpha)
  logrank <- survdiff(func, data = data)
  HR <- km.HR(logrank, alpha)
  p <- km.p(logrank)
  km$call$formula <- func
  ggsurv <- ggsurvplot(km, data, conf.int = conf.int)
  ggsurv$plot <- ggsurv$plot +
    ggplot2::annotate(
      "text",
      x = Inf, y = Inf,
      vjust = 1, hjust = 1,
      label = paste('HR = ', round(HR$HR,2), '\n', 
                    round(100*alpha), '%CI = (', round(HR$CI_low,2), 
                    ',', round(HR$CI_high,2), ')\n p = ', round(p,4), 
                    '\n # of group 1 = ', km[["n"]][1],
                    '\n # of group 2 = ', km[["n"]][2],
                    sep = ""),
      size = 5
    )
  ggsurv
}


surplot.3 <- function(km_results, data, logrank1, logrank2, alpha=.95, conf.int=TRUE){
  HR1 <- km.HR(logrank1, alpha)
  p1 <- km.p(logrank1)
  HR2 <- km.HR(logrank2, alpha)
  p2 <- km.p(logrank2)
  ggsurv <- ggsurvplot(km_results, data = data, conf.int = TRUE)
  a <- paste('cluster 1 vs. 3 (HR = ', rnd(HR1$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR1$CI_low,2), ',', rnd(HR1$CI_high,2), '); p = ', rnd(p1,3), ")", sep = "")
  b <- paste('cluster 2 vs. 3 (HR = ', rnd(HR2$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR2$CI_low,2), ',', rnd(HR2$CI_high,2), '); p = ', rnd(p2,3), ")", sep = "")
  ggsurv$plot <- ggsurv$plot +
    ggplot2::annotate(
      "text",
      x = Inf, y = Inf,
      vjust = 1, hjust = 1,
      label = paste(a,"\n",b, sep = ""),
      size = 5
    )
  ggsurv
}


surplot.4 <- function(km_results, data, logrank1, logrank2, logrank3, alpha=.95, conf.int=TRUE){
  HR1 <- km.HR(logrank1, alpha)
  p1 <- km.p(logrank1)
  HR2 <- km.HR(logrank2, alpha)
  p2 <- km.p(logrank2)
  HR3 <- km.HR(logrank3, alpha)
  p3 <- km.p(logrank3)
  ggsurv <- ggsurvplot(km_results, data = data, 
                       conf.int = conf.int, palette=c("palegreen3", "steelblue1", "orange", "red"))
  
  a <- paste('group 1 vs. 4 (HR = ', rnd(HR1$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR1$CI_low,2), ',', rnd(HR1$CI_high,2), '); p = ', rnd(p1,3), ")", sep = "")
  b <- paste('group 2 vs. 4 (HR = ', rnd(HR2$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR2$CI_low,2), ',', rnd(HR2$CI_high,2), '); p = ', rnd(p2,3), ")", sep = "")
  c <- paste('group 3 vs. 4 (HR = ', rnd(HR3$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR3$CI_low,2), ',', rnd(HR3$CI_high,2), '); p = ', rnd(p3,3), ")", sep = "")
  ggsurv$plot <- ggsurv$plot +
    ggplot2::annotate(
      "text",
      x = Inf, y = Inf,
      vjust = 1, hjust = 1,
      label = paste(a,"\n",b, "\n", c, sep = ""),
      size = 5
    )
  ggsurv
}


rnd <- function(x, c){
  formatC( round(x, c), format='f', digits=c)
}


min_max_scale <- function(x){(x-min(x))/(max(x)-min(x))}


boot_km <- function(df, func, n){
  results_p <- c()
  results_hr <- c()
  for (i in 1:n){
    dt <- sample(1:nrow(df), replace = TRUE)
    resample <- df[dt,]
    logrank <- survdiff(func, data = resample)
    results_p[i] <- pchisq(logrank$chisq, 1, lower.tail = FALSE)
    results_hr[i] <- (logrank[["obs"]][2]/logrank[["exp"]][2])/(logrank[["obs"]][1]/logrank[["exp"]][1])
  }
  return(list(results_p, results_hr))
}


boot_vae_km <- function(latent, df, func, n){
  results_p <- c()
  results_hr <- c()
  for (i in 1:n){
    dt <- sample(1:nrow(df), replace = TRUE)
    resample <- df[dt,]
    resample_latent <- latent[dt,]
    resample['cluster'] <- kmeans(resample_latent, centers = 2, nstart = 20)$cluster
    logrank <- survdiff(func, data = resample)
    results_p[i] <- pchisq(logrank$chisq, 1, lower.tail = FALSE)
    results_hr[i] <- (logrank[["obs"]][2]/logrank[["exp"]][2])/(logrank[["obs"]][1]/logrank[["exp"]][1])
  }
  return(list(results_p, results_hr))
}


k.medoids <- function(df_train,df_test){
  gower_dist.train <- daisy(df_train, metric = "gower")
  pam.res.train <- pam(gower_dist.train, diss=TRUE, 2)
  pam.res.train$clustering
  m <- pam.res.train$medoids
  medoid <- df_train[m,]
  names(medoid) <- names(df_test)
  df_test <- rbind(medoid, df_test)
  gower_dist.test <- daisy(df_test, metric = "gower")
  pam.res.test <- pam(gower_dist.test, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
  return(list(pam.res.train$clustering, pam.res.test$clustering[-c(1,2)]))
}


dist_similarity <- function(func, data, alpha=0.95){
  temp <- data
  temp1 <- temp %>% mutate(group = ifelse(MS_group==1, "MS_1", "MS_0"))
  temp2 <- temp %>% mutate(group = ifelse(x==1, "VAE_1", "VAE_0"))
  temp <- rbind(temp1, temp2)
  temp3 <- temp %>% filter(group=="MS_1" | group=="VAE_1")
  temp4 <- temp %>% filter(group=="MS_0" | group=="VAE_0")
  logrank1 <- survdiff(func, data = temp1)
  logrank2 <- survdiff(func, data = temp2)
  logrank3 <- survdiff(func, data = temp3)
  logrank4 <- survdiff(func, data = temp4)
  HR1 <- km.HR(logrank1, alpha)
  p1 <- km.p(logrank1)
  HR2 <- km.HR(logrank2, alpha)
  p2 <- km.p(logrank2)
  HR3 <- km.HR(logrank3, alpha)
  p3 <- km.p(logrank3)
  HR4 <- km.HR(logrank4, alpha)
  p4 <- km.p(logrank4)
  km <- survfit(formula = func, data = temp)
  km$call$formula <- func
  ggsurv <- ggsurvplot(km, temp, conf.int = FALSE, palette=c("palegreen3", "steelblue1", "orange", "red"))
  
  a <- paste('MS_0 vs. MS_1 (HR = ', rnd(HR1$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR1$CI_low,2), ',', rnd(HR1$CI_high,2), '); p = ', rnd(p1,3), ")", sep = "")
  b <- paste('VAE_0 vs. VAE_1 (HR = ', rnd(HR2$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR2$CI_low,2), ',', rnd(HR2$CI_high,2), '); p = ', rnd(p2,3), ")", sep = "")
  c <- paste('MS_1 vs. VAE_1 (HR = ', rnd(HR3$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR3$CI_low,2), ',', rnd(HR3$CI_high,2), '); p = ', rnd(p3,3), ")", sep = "")
  d <- paste('MS_0 vs. VAE_0 (HR = ', rnd(HR4$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR4$CI_low,2), ',', rnd(HR4$CI_high,2), '); p = ', rnd(p4,3), ")", sep = "")
  ggsurv$plot <- ggsurv$plot +
    ggplot2::annotate(
      "text",
      x = Inf, y = Inf,
      vjust = 1, hjust = 1,
      label = paste(a,"\n",b, "\n", c,"\n", d, sep = ""),
      size = 4
    )
  ggsurv
}


generate_label <- function(cancer, df, m, q){
  tmp <- df %>% filter(cancerTypeId==cancer)
  tmp$score <- 0
  n <- nrow(tmp)
  t <- 0
  for (i in 1:m){
    dt <- sample(1:n, n/2, replace = FALSE)
    tmp[dt,"group"] <- 1
    tmp[-dt,"group"] <- 0
    logrank <- survdiff(Surv(OS_MONTHS, OS_STATUS) ~ group, data = tmp)
    p <- km.p(logrank)
    if (p<0.05){
      t <- t+1
      if (km.HR(logrank)[[1]]>=1){
        tmp <- tmp %>% mutate(score=score+group)
      }else{
        tmp <- tmp %>% mutate(score=score+1-group)
      }
    }
  }
  low <- quantile(tmp$score, probs = c(q))[[1]]
  high <- quantile(tmp$score, probs = c(1-q))[[1]]
  tmp <- tmp %>%
    mutate(label = case_when(score < low ~ 1,
                             score > high ~ 3,
                             score >= low & score <= high ~ 2)) 
  df$label[df$cancerTypeId==cancer] <- plyr::mapvalues(df$id[df$cancerTypeId==cancer], from = tmp$id, to = tmp$label)
  return(df)
}


n.events <- function(alpha, beta, q, RH){
  A <- (qnorm(1-alpha/2)+ qnorm(1-beta))^2
  B <- (log(RH))^2*q*(1-q)
  return(A/B)
}

survival.power <- function(alpha, n, q, RH){
  B <- ((log(RH))^2)*q*(1-q)
  A <- n*B
  power <- pnorm(sqrt(A)-qnorm(1-alpha/2))
  return(power)
}

