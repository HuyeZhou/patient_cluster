source('util.R') 
library(tidyverse)

df_cbsp <- read_csv("data/cbsp_MTB_processed.csv")
cancer_list <-  read.csv("data/cancer_list.csv")
df_cbsp$cancerTypeId <- plyr::mapvalues(df_cbsp$cancerTypeId, from = cancer_list$cancer_idx, to = cancer_list$mapped_idx)
df_cbsp <- df_cbsp %>% filter(cancerTypeId!=0)
df_cbsp$label <- NA
for (i in 1:18){
  print(i)
  df_cbsp <- generate_label(i, df_cbsp, 10000, 0.1)
}
df_cbsp_2 <- df_cbsp %>% filter(label=="2")
for (i in 1:18){
  print(i)
  df_cbsp_2 <- generate_label(i, df_cbsp_2, 10000, 1/8)
}
df_cbsp_3 <- df_cbsp_2 %>% filter(label=="2")
for (i in 1:18){
  print(i)
  df_cbsp_3 <- generate_label(i, df_cbsp_3, 10000, 1/6)
}
df_cbsp_4 <- df_cbsp_3 %>% filter(label=="2")
for (i in 1:18){
  print(i)
  df_cbsp_4 <- generate_label(i, df_cbsp_4, 10000, 1/4)
}
df_cbsp_5 <- df_cbsp_4 %>% filter(label=="2")
for (i in 1:18){
  print(i)
  df_cbsp_5 <- generate_label(i, df_cbsp_5, 10000, 1/2)
}
a <- df_cbsp_5%>% group_by(cancerTypeId,label) %>% summarise(n=n())
a %>% group_by(cancerTypeId) %>% summarise(n=n())
df_cbsp_5$label <- as.numeric(df_cbsp_5$label)
df_cbsp_5[df_cbsp_5$cancerTypeId %in% c(2,9,13:16),]$label <- 5
df_cbsp_5[df_cbsp_5$label ==2,]$label <- 6
df_cbsp_5[df_cbsp_5$label ==3,]$label <- 6
df_cbsp_5[df_cbsp_5$label ==1,]$label <- 5
df_cbsp_4$label <- as.numeric(df_cbsp_4$label)
df_cbsp_4[df_cbsp_4$label ==1,]$label <- 4
df_cbsp_4[df_cbsp_4$label ==3,]$label <- 7
tmp <- rbind(df_cbsp_5, df_cbsp_4[df_cbsp_4$label != 2,])
df_cbsp_3$label <- as.numeric(df_cbsp_3$label)
df_cbsp_3[df_cbsp_3$label ==3,]$label <- 8
df_cbsp_3[df_cbsp_3$label ==1,]$label <- 3
tmp <- rbind(tmp, df_cbsp_3[df_cbsp_3$label != 2,])
df_cbsp_2$label <- as.numeric(df_cbsp_2$label)
df_cbsp_2[df_cbsp_2$label ==3,]$label <- 9
tmp <- rbind(tmp, df_cbsp_2[df_cbsp_2$label != 2,])
tmp[tmp$label ==1,]$label <- 2
df_cbsp$label <- as.numeric(df_cbsp$label)
df_cbsp[df_cbsp$label ==3,]$label <- 10
tmp <- rbind(tmp, df_cbsp[df_cbsp$label != 2,])
a <- tmp%>% group_by(cancerTypeId,label) %>% summarise(n=n())
a %>% group_by(cancerTypeId) %>% summarise(n=n())
df_cbsp$label <- 
  plyr::mapvalues(df_cbsp$id, from = tmp$id, to = tmp$label)
a <- cbsp%>% group_by(cancerTypeId,label) %>% summarise(n=n())
a %>% group_by(cancerTypeId) %>% summarise(n=n())
write_csv(df_cbsp, "data/cbsp_MTB_with_10_label_processed.csv")

tmp <- df_cbsp
df_cbsp <- tmp
df_cbsp$label <- as.numeric(df_cbsp$label)
df_cbsp_2$label <- as.numeric(df_cbsp_2$label)
df_cbsp_2$label <- df_cbsp_2$label+1
df_cbsp$label[df_cbsp$label==3] <- df_cbsp$label[df_cbsp$label==3]+2
df_cbsp$label[df_cbsp$label==2] <- 
  plyr::mapvalues(df_cbsp$id[df_cbsp$label==2], from = df_cbsp_2$id, to = df_cbsp_2$label)
table(df_cbsp$label)
a <- df_cbsp %>% group_by(cancerTypeId,label) %>% summarise(n=n())
write_csv(df_cbsp, "data/cbsp_MTB_with_5_label_processed.csv")
df_cbsp$label <- as.numeric(df_cbsp$label)
df_cbsp$label[df_cbsp$cancerTypeId==2] <- 
  plyr::mapvalues(df_cbsp$label[df_cbsp$cancerTypeId==2], 
                  from = c(1:5, 7:10), 
                  to = c(1:9))
df_cbsp$label[df_cbsp$cancerTypeId==9] <- 
  plyr::mapvalues(df_cbsp$label[df_cbsp$cancerTypeId==9], 
                  from = c(1:5, 7:10), 
                  to = c(1:9))
df_cbsp$label[df_cbsp$cancerTypeId==13] <- 
  plyr::mapvalues(df_cbsp$label[df_cbsp$cancerTypeId==13], 
                  from = c(1:5, 7:10), 
                  to = c(1:9))
df_cbsp$label[df_cbsp$cancerTypeId==15] <- 
  plyr::mapvalues(df_cbsp$label[df_cbsp$cancerTypeId==15], 
                  from = c(1:5, 7:10), 
                  to = c(1:9))
df_cbsp$label[df_cbsp$cancerTypeId==14] <- 
  plyr::mapvalues(df_cbsp$label[df_cbsp$cancerTypeId==14], 
                  from = c(1, 5, 10), 
                  to = c(1:3))
df_cbsp$label[df_cbsp$cancerTypeId==16] <- 
  plyr::mapvalues(df_cbsp$label[df_cbsp$cancerTypeId==16], 
                  from = c(1,2,5,9,10), 
                  to = c(1:5))


table(df_cbsp$label[df_cbsp$cancerTypeId==14])
table(df_cbsp$label[df_cbsp$cancerTypeId==16])


