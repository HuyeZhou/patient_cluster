library(openxlsx)
library(stringr)
library(plyr)
library(cluster)
library(factoextra)
library(survival)
library(survminer)
library(Rtsne)
library(ggplot2)
source('util.R')
library(tidyverse)
# whole set of MTB patients and application on Ipredict

df_MTB <- read.csv("data/VAE_REG_MTB_1000/MTB_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_1000/IPredict_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)


df_MTB <- read.csv("data/VAE_REG_MTB_no_MS_500/MTB_no_MS_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_no_MS_500/IPredict_no_MS_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_MTB <- read.csv("data/VAE_REG_MTB_mutations_500/MTB_mutations_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_mutations_500/IPredict_mutations_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_MTB <- read.csv("data/VAE_REG_MTB_MS_only_1000/MTB_MS_only_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_MS_only_1000/IPredict_MS_only_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_MTB <- read.csv("data/VAE_REG_MTB_MS_only_OS_1000/train_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_MS_only_OS_1000/test_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_MTB <- read.csv("data/VAE_REG_MTB_mutations_IPredict_mutations_OS_1000/train_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_mutations_IPredict_mutations_OS_1000/test_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_MTB <- read.csv("data/VAE_REG_MTB_age_1000/train_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_MTB_age_1000/test_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)


df_MTB <- read.csv("data/VAE_REG_Genie_MTB_test_MTB_Genie_MTB_test_IPredict_PFS_1000/train_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_Genie_MTB_test_MTB_Genie_MTB_test_IPredict_PFS_1000/test_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_MTB <- read.csv("data/VAE_REG_Genie_MTB_test_MTB_Genie_MTB_test_IPredict_OS_500/train_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
print(km.OS.95)

df_IP <- read.csv("data/VAE_REG_Genie_MTB_test_MTB_Genie_MTB_test_IPredict_OS_500/test_ds_with_assignment.csv")
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
print(km.OS.95)

df_Genie <- read.csv("data/VAE_REG_Genie_MTB_test_MTB_OS_500/Genie_breast_cancer_ds_with_assignment.csv")
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_Genie, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ group, data = df_Genie)
surplot(km.OS.95, df_Genie, logrank)
print(km.OS.95)



df_MTB <- read.csv("data/VAE_REG_Genie_MTB_test_MTB_Genie_MTB_test_IPredict_OS_500/train_ds_with_assignment.csv")
op <- par(mfrow = c(1, 2))
hist((df_MTB %>% filter(group==0))$Diagnosis,18, xlim=c(0,20), ylim=c(0,100))
hist((df_MTB %>% filter(group==1))$Diagnosis,18, xlim=c(0,20), ylim=c(0,100))



# df_MTB <- read.csv("data/one_hot_MTB_data_clean.csv")
# base_features <- df_MTB[,c(6, 7, 9, 15, 11:14)]
# drugs <- df_MTB[,37:176]
# mutations <- df_MTB[,177:534]
# base_features$MS <- 0
# base_features$MS[base_features$MS1==1] <- 0.25
# base_features$MS[base_features$MS2==1] <- 0.5
# base_features$MS[base_features$MS3==1] <- 0.75
# base_features$MS[base_features$MS4==1] <- 1
# base_features <- base_features[,-c(5:8)]
# write.csv(cbind(base_features, mutations), "data/MTB_clean_train.csv", row.names = FALSE)
# write.csv(df_MTB$PFS_Months, "data/MTB_clean_PFS.csv", row.names = FALSE)

library(tidyverse)
df_MTB <- read_csv("data/VAE_REG_MTB_mutations_500/MTB_mutations_ds_with_assignment.csv")
df_MTB <- read.csv("data/VAE_REG_MTB_MS_only_1000/MTB_MS_only_ds_with_assignment.csv")
df_MTB <- read.csv("data/VAE_REG_MTB_mutations_IPredict_mutations_OS_1000/train_ds_with_assignment.csv")
df_MTB <- read.csv("data/VAE_REG_MTB_MS_only_OS_1000/train_ds_with_assignment.csv")
df_MTB$MS <- 0
df_MTB$MS[df_MTB$MS1==1] <- 0.25
df_MTB$MS[df_MTB$MS2==1] <- 0.5
df_MTB$MS[df_MTB$MS3==1] <- 0.75
df_MTB$MS[df_MTB$MS4==1] <- 1
group0 <- df_MTB %>% filter(group==0) %>% select(MS)
group1 <- df_MTB %>% filter(group==1) %>% select(MS)
op <- par(mfrow = c(1, 2))
hist(group0$MS,col=rgb(1,0,0,1/4), xlim=c(0.2,1),ylim=c(0,150))
hist(group1$MS,col=rgb(0,0.8,1,1/4), xlim=c(0.2,1),ylim=c(0,150))
par(op)

df_IP <- read.csv("data/VAE_REG_MTB_mutations_500/IPredict_mutations_ds_with_assignment.csv")
df_IP <- read.csv("data/VAE_REG_MTB_MS_only_1000/IPredict_MS_only_ds_with_assignment.csv")
df_IP <- read.csv("data/VAE_REG_MTB_mutations_IPredict_mutations_OS_1000/test_ds_with_assignment.csv")
df_IP <- read.csv("data/VAE_REG_MTB_MS_only_OS_1000/test_ds_with_assignment.csv")
df_IP$MS <- 0
df_IP$MS[df_IP$MS1==1] <- 0.25
df_IP$MS[df_IP$MS2==1] <- 0.5
df_IP$MS[df_IP$MS3==1] <- 0.75
df_IP$MS[df_IP$MS4==1] <- 1
group0 <- df_IP %>% filter(group==0) %>% select(MS)
group1 <- df_IP %>% filter(group==1) %>% select(MS)
hist(group0$MS,col=rgb(1,0,0,1/4), xlim=c(0.2,1),ylim=c(0,25))
hist(group1$MS,col=rgb(0,0.8,1,1/4), xlim=c(0.2,1),ylim=c(0,25))
par(op)





latent_MTB <- read.csv("data/MTB_mutations_PFS_MTB_latent.csv")
df_MTB <-  read_csv("data/MTB_ds.csv")
df_MTB$count <- rowSums(df_MTB[,177:534])
df_MTB <- df_MTB %>% mutate(N_mutations=1*(count<5))
df_MTB$Age <- round(100*df_MTB$Age)
df_MTB <- df_MTB %>% mutate(
  age_group = ifelse(Age >= 61, 0, 1),
  Sex = ifelse(Sex == 1, 0, 1),
  MS = ifelse(df_MTB$MS3 + df_MTB$MS4 == 1, 0, 1),
  GI = ifelse(GI == 1, 0, 1),
  Number_of_lines = ifelse(Number_of_lines == 1, 0, 1),
  Immunotherapy = ifelse(Immnunotherapy == 1, 0, 1),
  MS = case_when(MS1 == 1 ~ 0.25,
                 MS2 == 1 ~ 0.5,
                 MS3 == 1 ~ 0.75,
                 MS4 == 1 ~ 1)
) %>% mutate(matched_mutations = count * MS)
df_MTB <- cbind(df_MTB[,c(2:5,535, 538, 540)], latent_MTB)
df_MTB <-
  df_MTB %>% mutate(
    x0 = ifelse(X0>0,1,0),
    x1 = ifelse(X1>0,1,0),
    x2 = ifelse(X2>0,1,0),
    x3 = ifelse(X3>0,1,0),
    x4 = ifelse(X4>0,1,0),
    x5 = ifelse(X5<0,1,0),
    x6 = ifelse(X6>0,1,0),
    x7 = ifelse(X7>0,1,0),
    MS_group = ifelse(MS>0.5,1,0),
  )
coxph(Surv(PFS_Months, PFS_Censoring_Status) ~ MS_group + x5, df_MTB)
coxph(Surv(OS_Months, OS_Censoring_Status) ~ MS_group + x5, df_MTB)
surplot(Surv(OS_Months, OS_Censoring_Status) ~ x5, df_MTB)
par(mfrow = c(1, 2))
library(ggplot2)

ggplot(df_MTB, aes(x = x5, fill = factor(MS_group))) +
  geom_bar(position = position_dodge2(preserve = "single")) +
  theme(plot.title = element_text(hjust = 0.5),
        legend.position = "bottom")

temp <- df_MTB
temp1 <- temp %>% mutate(group = ifelse(MS_group==1, "MS_1", "MS_0"))
temp2 <- temp %>% mutate(group = ifelse(x5==1, "VAE_1", "VAE_0"))
temp <- rbind(temp1, temp2)
temp3 <- temp %>% filter(group=="MS_1" | group=="VAE_1")
temp4 <- temp %>% filter(group=="MS_0" | group=="VAE_0")
alpha = 0.95
logrank1 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = temp1)
logrank2 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = temp2)
logrank3 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = temp3)
logrank4 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = temp4)
HR1 <- km.HR(logrank1, alpha)
p1 <- km.p(logrank1)
HR2 <- km.HR(logrank2, alpha)
p2 <- km.p(logrank2)
HR3 <- km.HR(logrank3, alpha)
p3 <- km.p(logrank3)
HR4 <- km.HR(logrank4, alpha)
p4 <- km.p(logrank4)
ggsurv <- ggsurvplot(survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ group, data = temp), conf.int = FALSE, palette=c("palegreen3", "steelblue1", "orange", "red"))

a <- paste('MS_0 vs. MS_1 (HR = ', rnd(HR1$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR1$CI_low,2), ',', rnd(HR1$CI_high,2), '); p = ', rnd(p1,3), ")", sep = "")
b <- paste('VAE_0 vs. VAE_1 (HR = ', rnd(HR2$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR2$CI_low,2), ',', rnd(HR2$CI_high,2), '); p = ', rnd(p2,3), ")", sep = "")
c <- paste('MS_1 vs. VAE_1 (HR = ', rnd(HR3$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR3$CI_low,2), ',', rnd(HR3$CI_high,2), '); p = ', rnd(p3,3), ")", sep = "")
d <- paste('MS_0 vs. VAE_0 (HR = ', rnd(HR4$HR,2), '; ', round(100*alpha), '%CI = (', rnd(HR4$CI_low,2), ',', rnd(HR4$CI_high,2), '); p = ', rnd(p4,3), ")", sep = "")
ggsurv$plot <- ggsurv$plot +
  ggplot2::annotate(
    "text",
    x = Inf, y = Inf,
    vjust = 1, hjust = 1,
    label = paste(a,"\n",b, "\n", c,"\n", d, sep = ""),
    size = 4
  )
ggsurv


pairs(df_MTB[,c(1, 5:15)]) 
lm_PFS <- lm(OS_Months~., data = df_MTB[,c(3, 5:15)])
summary(lm_PFS)

df_IP <- read.csv("data/one_hot_Ipredict_data.csv")
df_IP$count <- rowSums(df_IP[,155:512])
df_IP <- df_IP[,c(1:5,8,14,520)]
df_IP <-
  df_IP %>% mutate(
    MS = case_when(
      IP_score <= 0.25 ~ 0.25,
      IP_score <= 0.5 & IP_score > 0.25 ~ 0.5,
      IP_score <= 0.75 & IP_score > 0.5 ~ 0.75,
      IP_score <= 1 & IP_score > 0.75 ~ 1
    ),
    Number_of_lines = ifelse(Prior.Lines.of.Therapy == 1, 0, 1),
    matched_mutations = count * MS
  )
latent_IP <- read.csv("data/MTB_mutations_PFS_IP_latent.csv")
df_IP <- cbind(df_IP[,c(2:5, 8,9, 11)], latent_IP)
pairs(df_IP[,c(1, 5:15)]) 
df_IP$predicted <-  predict(lm_PFS, df_IP[,c(5:15)])
df_IP <- df_IP %>% mutate(group = ifelse(predicted> median(df_IP$predicted), 1,0))
df_IP <-
  df_IP %>% mutate(
    x0 = ifelse(X0>0,1,0),
    x1 = ifelse(X1>0,1,0),
    x2 = ifelse(X2>0,1,0),
    x3 = ifelse(X3>0,1,0),
    x4 = ifelse(X4>0,1,0),
    x5 = ifelse(X5>0,1,0),
    x6 = ifelse(X6>0,1,0),
    x7 = ifelse(X7>0,1,0),
    MS_group = ifelse(MS>0.5,1,0),
  )

coxph(Surv(PFS_Months, PFS_Censoring_Status) ~ MS_group + x5, df_IP)
coxph(Surv(OS_Months, OS_Censoring_Status) ~ MS_group + x5, df_IP)
surplot(Surv(OS_Months, OS_Censoring_Status) ~ group, df_IP)
surplot(Surv(PFS_Months, PFS_Censoring_Status) ~ group, df_IP)



results_vae_IP_PFS <- boot_vae_km(latent_IP, df_IP, Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, 500)
results_vae_IP_OS <- boot_vae_km(latent_IP, df_IP, Surv(OS_Months, OS_Censoring_Status) ~ cluster, 500)
par(mfrow = c(1, 2))
hist(results_vae_IP_PFS[[1]],xlim=c(0,1), breaks=c(0:100)/100,ylim=c(0,500), 
     main = paste('VAE_PFS_', round(sum(results_vae_IP_PFS[[1]]<0.05)/5,2), '%',sep = ""))
abline(v = 0.05, col="red", lwd=3, lty=2)
hist(results_vae_IP_OS[[1]],xlim=c(0,1), breaks=c(0:100)/100,ylim=c(0,500), 
     main = paste('VAE_OS_', round(sum(results_vae_IP_OS[[1]]<0.05)/5,2), '%',sep = ""))
abline(v = 0.05, col="red", lwd=3, lty=2)
source('util.R')
set.seed(123)
df_IP$MS <- 0
df_IP$MS[df_IP$MS1==1] <- 0.25
df_IP$MS[df_IP$MS2==1] <- 0.5
df_IP$MS[df_IP$MS3==1] <- 0.75
df_IP$MS[df_IP$MS4==1] <- 1
df_IP <- df_IP %>% mutate(MS_group= ifelse(MS<=0.5, 1,2))
kmean_IP <- kmeans(latent_IP, centers = 2, nstart = 25)
set.seed(123)
tsne_plot(latent_IP, df_IP$MS_group)
set.seed(123)
df_IP$cluster <- kmean_IP$cluster
tsne_plot(latent_IP, kmean_IP$cluster)
hist((df_IP %>% filter(cluster==1))$MS)
hist((df_IP %>% filter(cluster==2))$MS)



kmean_MTB <- kmeans(cbind(latent_MTB, df_MTB$MS), centers = 2, nstart = 1)
df_MTB['cluster'] <- kmean_MTB$cluster
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)
kmean_IP <- kmeans(cbind(latent_IP, df_IP$MS), centers = 2, nstart = 1)
df_IP['cluster'] <- kmean_IP$cluster
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

cluster1 <- df_MTB %>% filter(cluster==1) %>% select(MS)
cluster2 <- df_MTB %>% filter(cluster==2) %>% select(MS)
op <- par(mfrow = c(1, 2))
hist(cluster1$MS,col=rgb(1,0,0,1/4), xlim=c(0.2,1),ylim=c(0,200))
hist(cluster2$MS,col=rgb(0,0.8,1,1/4), xlim=c(0.2,1),ylim=c(0,200))
par(op)

cluster1 <- df_IP %>% filter(cluster==1) %>% select(MS)
cluster2 <- df_IP %>% filter(cluster==2) %>% select(MS)
op <- par(mfrow = c(1, 2))
hist(cluster1$MS,col=rgb(1,0,0,1/4), xlim=c(0.2,1),ylim=c(0,20))
hist(cluster2$MS,col=rgb(0,0.8,1,1/4), xlim=c(0.2,1),ylim=c(0,20))
par(op)
