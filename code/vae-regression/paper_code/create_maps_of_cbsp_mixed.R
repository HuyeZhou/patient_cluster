library(tidyverse)
library(plyr)

# map correct cancer types for mixed cases

cbsp_mixed_cancer_types <- read.csv("data/cbsp_cancer_type_for_mixed_cases.csv")
df_cbsp <- read.csv("data/cbsp_MTB_clinical_processed.csv")
df_cbsp <- separate(df_cbsp, id, c("studyId", "patientId"), "[.]")
cbsp_cancer_list <-  read.csv("data/cancer_list.csv")
mixed_cancer_list <- unique(cbsp_mixed_cancer_types$value)
mixed_cancer_list <- data.frame( "mapped_idx"=0, "name"=mixed_cancer_list)
for (i in 1:nrow(mixed_cancer_list)){
  name <- mixed_cancer_list$name[i]
  cancer_name <- strsplit(name," ")[[1]][1]
  similar_idx <- agrep(cancer_name,cbsp_cancer_list$name)
  ids <- cbsp_cancer_list$mapped_idx[similar_idx]
  if (length(unique(ids))==1){
    mixed_cancer_list$mapped_idx[i] <- unique(ids)
  }
  else {
    print(paste("the cancer name is '", 
                name,
                "', the similiar names are '", 
                paste(cbsp_cancer_list$name[similar_idx], collapse = ', '), 
                sep=''))
  }
}
# write.csv(mixed_cancer_list, "data/cancer_list_mixed_cases.csv", row.names = FALSE)

cbsp_mixed_cancer_types <- read.csv("data/cbsp_cancer_type_for_mixed_cases.csv")
cbsp_cancer_list <-  read.csv("data/cancer_list.csv")
cbsp_mixed_cancer_types <- within(cbsp_mixed_cancer_types,  id <- paste(studyId, patientId, sep="."))
cbsp_mixed_cancer_types <- cbsp_mixed_cancer_types[,c(4,3)]
cbsp_mixed_cancer_types <- cbsp_mixed_cancer_types[!duplicated(cbsp_mixed_cancer_types$id),]
mixed_cancer_list <- read.csv("data/cancer_list_mixed_cases.csv")
df_cbsp <- read.csv("data/cbsp_MTB_clinical_processed.csv")
df_cbsp$cancer_type <- mapvalues(df_cbsp$cancerTypeId, from = cbsp_cancer_list$cancer_idx, to = cbsp_cancer_list$mapped_idx)
df_cbsp_mixed <- df_cbsp %>% filter(cancer_type==0)
df_cbsp_mixed <- merge(df_cbsp_mixed, cbsp_mixed_cancer_types, by="id")
df_cbsp_mixed$cancer_type <- mapvalues(df_cbsp_mixed$value, from = mixed_cancer_list$name, to = mixed_cancer_list$mapped_idx)
df_cbsp_mixed <- df_cbsp_mixed[,-13]
df_cbsp_mapped <- rbind(df_cbsp_mixed, df_cbsp%>% filter(cancer_type!=0))
write.csv(df_cbsp_mapped, "data/cbsp_MTB_complete_clinical_processed.csv", row.names = FALSE)




cbsp_mixed_cancer_types <- read.csv("data/cbsp_cancer_type_for_mixed_cases.csv")
cbsp_cancer_list <-  read.csv("data/cancer_list.csv")
cbsp_mixed_cancer_types <- within(cbsp_mixed_cancer_types,  id <- paste(studyId, patientId, sep="."))
cbsp_mixed_cancer_types <- cbsp_mixed_cancer_types[,c(4,3)]
cbsp_mixed_cancer_types <- cbsp_mixed_cancer_types[!duplicated(cbsp_mixed_cancer_types$id),]
mixed_cancer_list <- read.csv("data/cancer_list_mixed_cases.csv")
df_cbsp <- read.csv("data/cbsp_processed.csv")
df_cbsp$cancer_type <- mapvalues(df_cbsp$cancerTypeId, from = cbsp_cancer_list$cancer_idx, to = cbsp_cancer_list$mapped_idx)
df_cbsp_mixed <- df_cbsp %>% filter(cancer_type==0)
df_cbsp_mixed <- merge(df_cbsp_mixed, cbsp_mixed_cancer_types, by="id")
df_cbsp_mixed$cancer_type <- mapvalues(df_cbsp_mixed$value, from = mixed_cancer_list$name, to = mixed_cancer_list$mapped_idx)
df_cbsp_mixed <- df_cbsp_mixed[,-7]
df_cbsp_mapped <- rbind(df_cbsp_mixed, df_cbsp%>% filter(cancer_type!=0))
write.csv(df_cbsp_mapped, "data/cbsp_complete_processed.csv", row.names = FALSE)


df_cbsp_mapped$cancer_type <- as.numeric(df_cbsp_mapped$cancer_type)
cancer_list <-  read.csv("data/cancer_list.csv")
MTB_cancer <- read.csv("data/MTB_cancer.csv")
cancer_count <- count(df_cbsp_mapped$cancer_type)
cancer_count$x <- mapvalues(cancer_count$x, from = MTB_cancer$idx, to = MTB_cancer$name)
cancer_count$freq <- round(cancer_count$freq/sum(cancer_count$freq),3)
ggbarplot(cancer_count, x = "x", y = "freq",
          fill = "lightgray", 
          xlab = "cancer types", ylab = "Count", caption = "CBSP",
          label = TRUE, label.pos = "out",
          x.text.angle = 45  # x axis text rotation angle
)
