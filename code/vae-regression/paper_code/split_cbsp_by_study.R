library(tidyverse)
library(plyr)


df_cbsp <- read_csv("data/cbsp_complete_processed.csv")
df_cbsp <- separate(df_cbsp, id, c("studyId", "patientId"), "[.]")
df_cbsp <- df_cbsp[!duplicated(df_cbsp[,c(2,6)]),]
study_list <- read_csv("data/cbsp_study_list_new.csv")[,-1]
train_study <- study_list$study_id[study_list$subset==0]
test_study <- study_list$study_id[study_list$subset==1]
df_cbsp_train <- df_cbsp %>% filter(studyId %in% train_study)
df_cbsp_test <- df_cbsp %>% filter(studyId %in% test_study)
df_cbsp_train$id <- paste(df_cbsp_train$studyId, df_cbsp_train$patientId, sep=".")
df_cbsp_test$id <- paste(df_cbsp_test$studyId, df_cbsp_test$patientId, sep=".")
df_cbsp_train <- df_cbsp_train[,c(8,3:7)]
df_cbsp_test <- df_cbsp_test[,c(8,3:7)]
write.csv(df_cbsp_train, "data/cbsp_complete_new_processed_train.csv", row.names = FALSE)
write.csv(df_cbsp_test, "data/cbsp_complete_new_processed_test.csv", row.names = FALSE)




library(tidyverse)
library(plyr)


df_cbsp_sorted <- read_csv("data/cbsp_complete_sorted_processed.csv")
df_cbsp_sorted <- separate(df_cbsp_sorted, id, c("studyId", "patientId"), "[.]")
df_cbsp_sorted <- df_cbsp_sorted[!duplicated(df_cbsp_sorted[,c(2,6)]),]
study_list <- read_csv("data/cbsp_study_list_new.csv")[,-1]
train_study <- study_list$study_id[study_list$subset==0]
test_study <- study_list$study_id[study_list$subset==1]
df_cbsp_train <- df_cbsp_sorted %>% filter(studyId %in% train_study)
df_cbsp_test <- df_cbsp_sorted %>% filter(studyId %in% test_study)
df_cbsp_train$id <- paste(df_cbsp_train$studyId, df_cbsp_train$patientId, sep=".")
df_cbsp_test$id <- paste(df_cbsp_test$studyId, df_cbsp_test$patientId, sep=".")
df_cbsp_train <- df_cbsp_train[,c(8,3:7)]
df_cbsp_test <- df_cbsp_test[,c(8,3:7)]
write.csv(df_cbsp_train, "data/cbsp_complete_new_sorted_processed_train.csv", row.names = FALSE)
write.csv(df_cbsp_test, "data/cbsp_complete_new_sorted_processed_test.csv", row.names = FALSE)






library(tidyverse)
library(plyr)
df_cbsp <- read_csv("data/cbsp_MTB_complete_clinical_processed.csv")
df_cbsp <- separate(df_cbsp, id, c("studyId", "patientId"), "[.]")
study_list <- read_csv("data/cbsp_study_list.csv")
train_study <- study_list$study_id[study_list$subset==0]
test_study <- study_list$study_id[study_list$subset==1]
df_cbsp_train <- df_cbsp %>% filter(studyId %in% train_study)
df_cbsp_test <- df_cbsp %>% filter(studyId %in% test_study)
df_cbsp_train$id <- paste(df_cbsp_train$studyId, df_cbsp_train$patientId, sep=".")
df_cbsp_test$id <- paste(df_cbsp_test$studyId, df_cbsp_test$patientId, sep=".")
df_cbsp_train <- df_cbsp_train[,c(14,3:6)]
df_cbsp_test <- df_cbsp_test[,c(14,3:6)]
write.csv(df_cbsp_train, "data/cbsp_MTB_complete_processed_train.csv", row.names = FALSE)
write.csv(df_cbsp_test, "data/cbsp_MTB_complete_processed_test.csv", row.names = FALSE)
