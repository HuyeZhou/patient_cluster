library(stringr)
library(plyr)
library(cluster)
library(factoextra)
library(survival)
library(survminer)
library(Rtsne)
library(ggplot2)
source('code/util.R')
df_MTB <- read.csv("data/one_hot_MTB_data_with_Ipredict_score.csv")
base_features <- df_MTB[, c(6, 7, 9, 15)]
drugs <- df_MTB[,37:176]
mutations <- df_MTB[,177:534]
df_IP <- read.csv("data/one_hot_Ipredict_data.csv")
base_features_IP <- df_IP[,c(6:8, 9)]
drugs_IP <- df_IP[,15:154]
mutations_IP <- df_IP[,155:512]
MS <- df_MTB[, 535:541]
MS_IP <- df_IP[,513:519]
# --------------------------------------------------------------------------------------------#
# KM on Maching score(same as MTB paper)
df_MTB$MS <- 0
df_MTB$MS[df_MTB$MS1==1] <- 4
df_MTB$MS[df_MTB$MS2==1] <- 3
df_MTB$MS[df_MTB$MS3==1] <- 2
df_MTB$MS[df_MTB$MS4==1] <- 1
group14 <- df_MTB[((df_MTB$MS==1)+(df_MTB$MS==4))==1,]
group24 <- df_MTB[((df_MTB$MS==2)+(df_MTB$MS==4))==1,]
group34 <- df_MTB[((df_MTB$MS==3)+(df_MTB$MS==4))==1,]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ MS, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank14 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ MS, data = group14)
logrank24 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ MS, data = group24)
logrank34 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ MS, data = group34)
surplot.4(km.PFS.95, df_MTB, logrank14, logrank24, logrank34, conf.int=FALSE, xlim=c(0,36), break.time.by = 6)
km.PFS.95
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ MS, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank14 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ MS, data = group14)
logrank24 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ MS, data = group24)
logrank34 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ MS, data = group34)
surplot.4(km.OS.95, df_MTB, logrank14, logrank24, logrank34, conf.int=FALSE, xlim=c(0,60), break.time.by = 12)
km.OS.95
# --------------------------------------------------------------------------------------------#
# metadata + mutations + Ipredict features + Ipredict score

fviz_nbclust(cbind(mutations, MS, base_features), pam, method = "silhouette") 
gower_dist <- daisy(cbind(mutations, MS, base_features), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(mutations, MS, base_features)[m,]

features_IP <- cbind(mutations_IP, MS_IP, base_features_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

# --------------------------------------------------------------------------------------------#
# metadata + mutations + Ipredict features

fviz_nbclust(cbind(mutations, MS[,1:6], base_features), pam, method = "silhouette") 
gower_dist <- daisy(cbind(mutations, MS[,1:6], base_features), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(mutations, MS[,1:6], base_features)[m,]

features_IP <- cbind(mutations_IP, MS_IP[,1:6], base_features_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

# --------------------------------------------------------------------------------------------#
# metadata + mutations + Ipredict features

fviz_nbclust(cbind(mutations, MS[,7], base_features), pam, method = "silhouette") 
gower_dist <- daisy(cbind(mutations, MS[,7], base_features), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(mutations, MS[,7], base_features)[m,]

features_IP <- cbind(mutations_IP, MS_IP[,7], base_features_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

# --------------------------------------------------------------------------------------------#
# metadata + mutations

fviz_nbclust(cbind(mutations, base_features), pam, method = "silhouette") 
gower_dist <- daisy(cbind(mutations, base_features), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(mutations, base_features)[m,]

features_IP <- cbind(mutations_IP, base_features_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

# --------------------------------------------------------------------------------------------#
# mutations + Ipredict features + Ipredict score

fviz_nbclust(cbind(mutations, MS), pam, method = "silhouette") 
gower_dist <- daisy(cbind(mutations, MS), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(mutations, MS)[m,]

features_IP <- cbind(mutations_IP, MS_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

# --------------------------------------------------------------------------------------------#
# mutations + Ipredict features + Ipredict score 3 cluster

fviz_nbclust(cbind(mutations,MS), pam, method = "silhouette") 
gower_dist <- daisy(cbind(mutations,MS), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 3)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
group13 <- df_MTB[df_MTB['cluster']!=2,]
group23 <- df_MTB[df_MTB['cluster']!=1,]
logrank1 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = group13)
logrank2 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = group23)
surplot.3(km.PFS.95, df_MTB, logrank1, logrank2)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank1 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = group13)
logrank2 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = group23)
surplot.3(km.OS.95, df_MTB, logrank1, logrank2)

m <- pam.res$medoids
medoid <- cbind(mutations,MS)[m,]

features_IP <- cbind(mutations_IP,MS_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 3,medoids = 1:3, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1:3)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
group13 <- df_IP[df_IP['cluster']!=2,]
group23 <- df_IP[df_IP['cluster']!=1,]
logrank1 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = group13)
logrank2 <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = group23)
surplot.3(km.PFS.95, df_IP, logrank1, logrank2)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank1 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = group13)
logrank2 <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = group23)
surplot.3(km.OS.95, df_IP, logrank1, logrank2)

# --------------------------------------------------------------------------------------------#
# metadata + Ipredict features + Ipredict score

fviz_nbclust(cbind(base_features, MS), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features, MS), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features, MS)[m,]

features_IP <- cbind(base_features_IP, MS_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)

# --------------------------------------------------------------------------------------------#
# metadata + Ipredict features

fviz_nbclust(cbind(base_features, MS[,1:6]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features, MS[,1:6]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features, MS[,1:6])[m,]

features_IP <- cbind(base_features_IP, MS_IP[,1:6])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
# --------------------------------------------------------------------------------------------#
# metadata + Ipredict scores

fviz_nbclust(cbind(base_features, MS[,7]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features, MS[,7]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features, MS[,7])[m,]

features_IP <- cbind(base_features_IP, MS_IP[,7])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
# --------------------------------------------------------------------------------------------#
# Ipredict features + Ipredict score

fviz_nbclust(cbind(MS), pam, method = "silhouette") 
gower_dist <- daisy(cbind(MS), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(MS)[m,]

features_IP <- cbind(MS_IP)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
# --------------------------------------------------------------------------------------------#
# Ipredict features

fviz_nbclust(cbind(MS[,1:6]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(MS[,1:6]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(MS[,1:6])[m,]

features_IP <- cbind(MS_IP[,1:6])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
# --------------------------------------------------------------------------------------------#
# Ipredict scores

fviz_nbclust(cbind(MS[,7]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(MS[,7]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)

df_MTB['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.PFS.95, df_MTB, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB)
surplot(km.OS.95, df_MTB, logrank)

m <- pam.res$medoids
medoid <- cbind(MS[,7])[m,]

features_IP <- cbind(MS_IP[,7])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.PFS.95, df_IP, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP)
surplot(km.OS.95, df_IP, logrank)
# --------------------------------------------------------------------------------------------#