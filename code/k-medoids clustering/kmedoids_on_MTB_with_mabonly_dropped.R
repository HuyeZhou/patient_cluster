library(tidyr)
library(openxlsx)
library(dplyr)
library(stringr)
library(plyr)
library(cluster)
library(factoextra)
library(survival)
library(survminer)
library(Rtsne)
library(ggplot2)
source('code/util.R')

df_MTB <- read.csv("data/one_hot_MTB_data.csv")
base_features <- df_MTB[,6:36]
drugs <- df_MTB[,37:176]
mutations <- df_MTB[,177:534]
drugs_mab_only <- drugs[rowSums(drugs[,109:140])!=0,][rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]
drugs_mab_only_names <- as.numeric(row.names(drugs_mab_only))
drugs_no_mab <- drugs[-drugs_mab_only_names,]
mutations_no_mab <- mutations[-drugs_mab_only_names,]
base_features_no_mab <- base_features[-drugs_mab_only_names,]

Study.ID <- df_MTB$Study.ID[-drugs_mab_only_names]
PFS.OS <- df_MTB[-drugs_mab_only_names,2:5]
df_MTB_no_mab <- cbind(Study.ID, PFS.OS, base_features_no_mab,mutations_no_mab,drugs_no_mab)

# --------------------------------------------------------------------------------------------#

# features mutations drugs
fviz_nbclust(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist, pam.res$clustering)

# --------------------------------------------------------------------------------------------#

# with age dropped

gower_dist.1 <- daisy(cbind(base_features_no_mab[,-1],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.1 <- pam(gower_dist.1, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.1'] <- pam.res.2.1$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.1, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.1, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.1, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.1, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.1, pam.res.2.1$clustering)

# --------------------------------------------------------------------------------------------#

# with sex dropped

gower_dist.2 <- daisy(cbind(base_features_no_mab[,-c(2)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.2 <- pam(gower_dist.2, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.2'] <- pam.res.2.2$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.2, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.2, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.2, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.2, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.2, pam.res.2.2$clustering)

# --------------------------------------------------------------------------------------------#
# with GI dropped
gower_dist.3 <- daisy(cbind(base_features_no_mab[,-c(3)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.3 <- pam(gower_dist.3, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.3'] <- pam.res.2.3$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.3, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.3, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.3, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.3, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.3, pam.res.2.3$clustering)

# --------------------------------------------------------------------------------------------#

# with number_of_lines dropped

gower_dist.4 <- daisy(cbind(base_features_no_mab[,-c(4)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.4 <- pam(gower_dist.4, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.4'] <- pam.res.2.4$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.4, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.4, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.4, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.4, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.4, pam.res.2.4$clustering)

# --------------------------------------------------------------------------------------------#

# with Immunotherapy dropped

gower_dist.5 <- daisy(cbind(base_features_no_mab[,-c(5)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.5 <- pam(gower_dist.5, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.5'] <- pam.res.2.5$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.5, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.5, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.5, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.5, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.5, pam.res.2.5$clustering)

# --------------------------------------------------------------------------------------------#

# with Maching score dropped

gower_dist.6 <- daisy(cbind(base_features_no_mab[,-c(6:9)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.6 <- pam(gower_dist.6, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.6'] <- pam.res.2.6$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.6, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.6, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.6, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.6, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.6, pam.res.2.6$clustering)

# --------------------------------------------------------------------------------------------#

# with Diagnosis dropped

gower_dist.7 <- daisy(cbind(base_features_no_mab[,-c(11:28)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.7 <- pam(gower_dist.7, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.7'] <- pam.res.2.7$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.7, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.7, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.7, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.7, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.7, pam.res.2.7$clustering)

# --------------------------------------------------------------------------------------------#

# with Compliance with recommendation of MTB dropped

gower_dist.8 <- daisy(cbind(base_features_no_mab[,-c(29:31)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.8 <- pam(gower_dist.8, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.8'] <- pam.res.2.8$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.8, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.8, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.8, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.8, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.8, pam.res.2.8$clustering)

# --------------------------------------------------------------------------------------------#

# with Compliance with matched treatment dropped

gower_dist.9 <- daisy(cbind(base_features_no_mab[,-c(10)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.9 <- pam(gower_dist.9, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.9'] <- pam.res.2.9$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.9, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.9, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.9, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.9, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.9, pam.res.2.9$clustering)

# --------------------------------------------------------------------------------------------#

# base_features mutations

gower_dist.10 <- daisy(cbind(base_features_no_mab,mutations_no_mab), metric = "gower")
pam.res.2.10 <- pam(gower_dist.10, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.10'] <- pam.res.2.10$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.10, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.10, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.10, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.10, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.10, pam.res.2.10$clustering)

# --------------------------------------------------------------------------------------------#

# drugs mutations

gower_dist.11 <- daisy(cbind(drugs_no_mab,mutations_no_mab), metric = "gower")
pam.res.2.11 <- pam(gower_dist.11, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.11'] <- pam.res.2.11$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.11, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.11, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.11, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.11, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.11, pam.res.2.11$clustering)

# --------------------------------------------------------------------------------------------#

# drugs metadata

gower_dist.12 <- daisy(cbind(base_features_no_mab,drugs_no_mab), metric = "gower")
pam.res.2.12 <- pam(gower_dist.12, diss=TRUE, 2)
df_MTB_no_mab['cluster_2.12'] <- pam.res.2.12$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.12, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.12, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.12, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.12, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist.12, pam.res.2.12$clustering)

# --------------------------------------------------------------------------------------------#

# common variables with Ipredict data (meta + drugs + mutations)

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],mutations_no_mab,drugs_no_mab), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist, pam.res$clustering)

# --------------------------------------------------------------------------------------------#

# common variables with Ipredict data (meta + mutations)

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],mutations_no_mab), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],mutations_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist, pam.res$clustering)

# --------------------------------------------------------------------------------------------#

# common variables with Ipredict data (meta + drugs)

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],drugs_no_mab), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist, pam.res$clustering)

# --------------------------------------------------------------------------------------------#

# common variables with Ipredict data (meta) 
# Age, Sex, # of proir lines, MS, matched treatment
# (align with MTB paper results)

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
tsne_plot(gower_dist, pam.res$clustering)

# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# Application on Ipredict data
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#
# --------------------------------------------------------------------------------------------#

df_IP <- read.csv("data/one_hot_Ipredict_data.csv")

base_features_IP <- df_IP[,c(6:8,10:13, 9, 14)]
drugs_IP <- df_IP[,15:154]
mutations_IP <- df_IP[,155:512]
drugs_IP_mab_only <- drugs_IP[rowSums(drugs_IP[,109:140])!=0,][rowSums(drugs_IP[rowSums(drugs_IP[,109:140])!=0, 1:108])==0,]
drugs_IP_mab_only_names <- as.numeric(row.names(drugs_IP_mab_only))
drugs_IP_no_mab <- drugs_IP[-drugs_IP_mab_only_names,]
mutations_IP_no_mab <- mutations_IP[-drugs_IP_mab_only_names,]
base_features_IP_no_mab <- base_features_IP[-drugs_IP_mab_only_names,]

Study.ID <- df_IP$Study.ID[-drugs_IP_mab_only_names]
PFS.OS <- df_IP[-drugs_IP_mab_only_names,2:5]
df_IP_no_mab <- cbind(Study.ID, PFS.OS, base_features_IP_no_mab,mutations_IP_no_mab,drugs_IP_no_mab)

# --------------------------------------------------------------------------------------------#
# all data
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)], mutations_no_mab,drugs_no_mab)[m,]

features_IP <- cbind(base_features_IP_no_mab[, -9], mutations_IP_no_mab, drugs_IP_no_mab)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# mata + mutations
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],mutations_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)], mutations_no_mab)[m,]

features_IP <- cbind(base_features_IP_no_mab[, -9], mutations_IP_no_mab)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# mata + drugs
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)],drugs_no_mab)[m,]

features_IP <- cbind(base_features_IP_no_mab[, -9], drugs_IP_no_mab)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# matadata only
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 5, 11:28, 29:31)])[m,]

features_IP <- cbind(base_features_IP_no_mab[, -9])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# matadata only with age dropped
fviz_nbclust(cbind(base_features_no_mab[,-c(1, 3, 5, 11:28, 29:31)]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(1, 3, 5, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(1, 3, 5, 11:28, 29:31)])[m,]

features_IP <- cbind(base_features_IP_no_mab[, -c(1,9)])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# matadata only with sex dropped
fviz_nbclust(cbind(base_features_no_mab[,-c(2, 3, 5, 11:28, 29:31)]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(2, 3, 5, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(2, 3, 5, 11:28, 29:31)])[m,]

features_IP <- cbind(base_features_IP_no_mab[, -c(2,9)])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# matadata only with # of prior lines dropped

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 4, 5, 11:28, 29:31)]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 4, 5, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 4, 5, 11:28, 29:31)])[m,]

features_IP <- cbind(base_features_IP_no_mab[, -c(3,9)])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# matadata only with ms dropped

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 5, 6:9, 11:28, 29:31)]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 6:9, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 5, 6:9, 11:28, 29:31)])[m,]

features_IP <- cbind(base_features_IP_no_mab[, -c(4:7,9)])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#
# matadata only with matched treatment dropped

fviz_nbclust(cbind(base_features_no_mab[,-c(3, 5, 10, 11:28, 29:31)]), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab[,-c(3, 5, 10, 11:28, 29:31)]), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)

m <- pam.res$medoids
medoid <- cbind(base_features_no_mab[,-c(3, 5, 10, 11:28, 29:31)])[m,]

features_IP <- cbind(base_features_IP_no_mab[, -c(8,9)])
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)


# --------------------------------------------------------------------------------------------#

#clustering directly on Ipredict
# common variables
gower_dist.IP.1 <- daisy(cbind(base_features_IP_no_mab[,-c(9)],mutations_IP_no_mab,drugs_IP_no_mab), metric = "gower")
pam.res.2.IP.1 <- pam(gower_dist.IP.1, diss=TRUE, 2)
df_IP_no_mab['cluster_2.1'] <- pam.res.2.IP.1$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.1, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.1, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.1, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.1, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)
print(km.OS.95)

# --------------------------------------------------------------------------------------------#
#clustering directly on Ipredict
# without Ipredict score
gower_dist.IP.2 <- daisy(cbind(base_features_IP_no_mab[,-c(4:7,9)],mutations_IP_no_mab,drugs_IP_no_mab), metric = "gower")
pam.res.2.IP.2 <- pam(gower_dist.IP.2, diss=TRUE, 2)
df_IP_no_mab['cluster_2.2'] <- pam.res.2.IP.2$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)
print(km.OS.95)

# --------------------------------------------------------------------------------------------#
#clustering directly on Ipredict
# with Ipredict score
gower_dist.IP.2 <- daisy(cbind(base_features_IP_no_mab[,-c(4:7)],mutations_IP_no_mab,drugs_IP_no_mab), metric = "gower")
pam.res.2.IP.2 <- pam(gower_dist.IP.2, diss=TRUE, 2)
df_IP_no_mab['cluster_2.2'] <- pam.res.2.IP.2$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster_2.2, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)
print(km.OS.95)

# --------------------------------------------------------------------------------------------#
# mutations only

gower_dist <- daisy(cbind(base_features_no_mab[,c(6:9)],mutations_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 4)

df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)

m <- pam.res$medoids
medoid <- mutations_no_mab[m,]

features_IP <- cbind(base_features_no_mab[,c(6:9)], mutations_IP_no_mab)
names(medoid) <- names(features_IP)
features_IP <- rbind(medoid, features_IP)
gower_dist.IP <- daisy(features_IP, metric = "gower")
pam.res.IP <- pam(gower_dist.IP, diss=TRUE, 2,medoids = 1:2, do.swap = FALSE)
df_IP_no_mab['cluster'] <- pam.res.IP$clustering[-c(1,2)]
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.PFS.95, df_IP_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_IP_no_mab)
surplot(km.OS.95, df_IP_no_mab, logrank)

