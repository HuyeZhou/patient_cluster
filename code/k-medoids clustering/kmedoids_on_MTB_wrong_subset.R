library(tidyr)
library(openxlsx)
library(dplyr)
library(stringr)
library(plyr)
library(cluster)
library(factoextra)
library(survival)
library(survminer)
library(Rtsne)
library(ggplot2)
source('code/util.R')

df_MTB <- read.csv("data/one_hot_MTB_data.csv")
base_features <- df_MTB[,6:36]
drugs <- df_MTB[,37:176]
mutations <- df_MTB[,177:534]
drugs_no_mab <- drugs[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]
mutations_no_mab <- mutations[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]
base_features_no_mab <- base_features[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]
Study.ID <- df_MTB$Study.ID[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0]
PFS.OS <- df_MTB[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,2:5]
df_MTB_no_mab <- cbind(Study.ID, PFS.OS, base_features_no_mab,mutations_no_mab,drugs_no_mab)

fviz_nbclust(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
df_MTB_no_mab['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
print(km.OS.95)

tsne_plot(gower_dist, pam.res$clustering)
df_MTB_IP <- read.csv("data/one_hot_MTB_data_with_Ipredict_score.csv")
df_MTB_no_mab$Study.ID <-as.integer(df_MTB_no_mab$Study.ID)

subset <- df_MTB_no_mab %>% filter(Study.ID %in% df_MTB_IP$Study.ID)
subset <- subset %>% mutate(MS_group=MS1+MS2)

base_features <- subset[,6:36]
drugs <- subset[,37:176]
mutations <- subset[,177:534]
drugs_no_mab <- drugs[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]
mutations_no_mab <- mutations[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]
base_features_no_mab <- base_features[! rowSums(drugs[rowSums(drugs[,109:140])!=0, 1:108])==0,]

km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ MS_group, data = subset, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ MS_group, data = subset)
surplot(km.PFS.95, subset, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ MS_group, data = subset, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ MS_group, data = subset)
surplot(km.OS.95, subset, logrank)

gower_dist <- daisy(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), metric = "gower")

fviz_nbclust(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), pam, method = "silhouette") 
gower_dist <- daisy(cbind(base_features_no_mab,mutations_no_mab,drugs_no_mab), metric = "gower")
pam.res <- pam(gower_dist, diss=TRUE, 2)
subset['cluster'] <- pam.res$clustering
km.PFS.95 <- survfit(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(PFS_Months, PFS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.PFS.95, df_MTB_no_mab, logrank)
print(km.PFS.95)
km.OS.95 <- survfit(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab, conf.type = "log-log", conf.int=.95)
logrank <- survdiff(Surv(OS_Months, OS_Censoring_Status) ~ cluster, data = df_MTB_no_mab)
surplot(km.OS.95, df_MTB_no_mab, logrank)
print(km.OS.95)